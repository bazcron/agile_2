let mongoose = require('mongoose');
//
let statementSchema = new mongoose.Schema({
        id: Number,
        statement: String,
        agree: Number,
        disagree:Number
    },
    { collection: 'statements' });

module.exports = mongoose.model('statements', statementSchema);
